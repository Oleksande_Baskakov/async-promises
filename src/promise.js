(function() {
  const symbolStatus = Symbol('Status');
  const symbolResult = Symbol('Result');
  const symbolOnFulfilledCallbacks = Symbol('onFulfilledCallbacks');
  const symbolOnRejectedCallbacks = Symbol('onRejectedCallbacks');
  const symbolChangePromiseStatus = Symbol('ChangePromiseStatus');

  const FULFILLED = 'fulfilled';
  const REJECTED = 'rejected';
  const PENDING = 'pending';

  function isIterable(obj) {
    if (obj === null || obj === undefined) {
      return false;
    }

    return typeof obj[Symbol.iterator] === 'function';
  }

  function rejectFunc(val) {
    if (this[symbolStatus] !== PENDING) {
      throw new TypeError();
    }

    this[symbolChangePromiseStatus](REJECTED, val);
  }

  function resolveFunc(val) {
    if (this[symbolStatus] !== PENDING) {
      throw new TypeError();
    }

    this[symbolChangePromiseStatus](FULFILLED, val);
  }

  class OwnPromise {
    [symbolStatus] = PENDING;
    [symbolResult];
    [symbolOnFulfilledCallbacks] = [];
    [symbolOnRejectedCallbacks] = [];
    constructor(executor) {
      if (typeof executor !== 'function') {
        throw new TypeError();
      }

      if (typeof this !== 'object') {
        throw new TypeError();
      }

      if (this[symbolStatus] !== PENDING) {
        throw new TypeError();
      }

      try {
        executor(resolveFunc.bind(this), rejectFunc.bind(this));
      } catch (e) {
        rejectFunc.call(this, e);
      }
    }


    static resolve(val) {
      if (typeof this !== 'function') {
        throw new TypeError();
      }

      if (this !== OwnPromise) {
        throw new TypeError();
      }

      if (val && val.constructor === OwnPromise) {
        return val;
      }

      return new OwnPromise(res => res(val));
    }

    static reject(err) {
      if (typeof this !== 'function') {
        throw new TypeError();
      }

      if (this !== OwnPromise) {
        throw new TypeError();
      }

      return new OwnPromise((res, rej) => rej(err));
    }

    static all(iterable) {
      if (this !== OwnPromise) {
        throw new TypeError();
      }

      if (!isIterable(iterable)) {
        return OwnPromise.reject(new TypeError('non-iterable'));
      }

      let resolveFuncHolder = null;
      let rejectFuncHolder = null;

      const returnedPromise = new OwnPromise(function(res, rej) {
        resolveFuncHolder = res;
        rejectFuncHolder = rej;
      });

      const arrOfResults = [];
      iterable.forEach(el => el.then(resolved => {
        arrOfResults.push(resolved);

        if (arrOfResults.length === iterable.length) {
          resolveFuncHolder(arrOfResults);
        }
      }, error => rejectFuncHolder(error)));

      if (iterable.length === 0) {
        resolveFuncHolder([]);
      }
      return returnedPromise;
    }

    static race(iterable) {
      if (this !== OwnPromise) {
        throw new TypeError();
      }

      if (!isIterable(iterable)) {
        return OwnPromise.reject(new TypeError('non-iterable'));
      }

      let resolveFuncHolder = null;
      let rejectFuncHolder = null;
      const result = new OwnPromise((res, rej) => {
        resolveFuncHolder = res;
        rejectFuncHolder = rej;
      });
      let firstResOrRejProm = null;

      iterable.forEach(el => {
        if (el[symbolStatus] !== PENDING) {
          if (!firstResOrRejProm) {
            firstResOrRejProm = el;
          }
        }
      });

      if (!firstResOrRejProm) {
        iterable.forEach(el => {
          setTimeout(() => {
            el.then(res => resolveFuncHolder(res), err => rejectFuncHolder(err));
          });
        });
      }

      return firstResOrRejProm || result;
    }

    [symbolChangePromiseStatus](status, val) {
      setTimeout(() => {
        this[symbolResult] = val;
        this[symbolStatus] = status;
        this[status === FULFILLED ? symbolOnFulfilledCallbacks : symbolOnRejectedCallbacks].forEach(el => el(val));
        this[symbolOnFulfilledCallbacks] = [];
        this[symbolOnRejectedCallbacks] = [];
      });
    }
    then(callback, callbackForError) {
      if (this.constructor !== OwnPromise) {
        throw new TypeError();
      }

      if (this[symbolStatus] === REJECTED && !callbackForError) {
        return this;
      }

      return new OwnPromise((res, rej) => {
        if (this[symbolStatus] === FULFILLED) {
          res(callback(this[symbolResult]));
        } else if (this[symbolStatus] === PENDING) {
          this[symbolOnFulfilledCallbacks].push(result => res(callback(result)));

          if (callbackForError) {
            this[symbolOnRejectedCallbacks].push(error => res(callbackForError(error)));
          } else {
            this[symbolOnRejectedCallbacks].push(rej);
          }
        } else {
          res(callbackForError(this[symbolResult]));
        }
      }
      );
    }

    catch(callback) {
      if (this[symbolStatus] === FULFILLED) {
        return OwnPromise.resolve();
      }

      return new OwnPromise((res, rej) => {
        if (this[symbolStatus] === REJECTED) {
          res(callback(this[symbolResult]));
        } else {
          this[symbolOnRejectedCallbacks].push(result => rej(callback(result)));
        }
      });
    }
  }

  module.exports = OwnPromise;
}());