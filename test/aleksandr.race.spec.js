const { assert, expect, done } = require('chai');
const OwnPromise = require('../src/promise');

function unexpectedReject(done) {
  return function() {
    done('Error: promise was expected to not reject, but did.');
  };
}

function unexpectedResolve(done) {
  return function() {
    done('Error: promise was expected to not resolve, but did.');
  };
}

describe('method race', function(done) {
  let myRes = null;
  const p = new Promise(res => {
    myRes = res;
  });

  // eslint-disable-next-line max-len
  it('should return fulfillded promise with result of undefined when in array was pending promise and undefined', function(done) {
    OwnPromise.race([p, undefined]).then(function(resolved) {
      expect(resolved).to.be.undefined;
    }, () => unexpectedReject(done))
      .then(done)
      .catch(done);
  });

  // eslint-disable-next-line max-len
  it('should return fulfillded promise with result of 1 when in array was fulfilled promise and undefined', function(done) {
    myRes(1);
    OwnPromise.race([p, undefined]).then(function(resolved) {
      expect(resolved).to.equal(1);
    }, () => unexpectedReject(done))
      .then(done)
      .catch(done);
  });
  describe('method should work with strings', function(done) {
    it('empty string in method should return a pending promise', function(done) {
      OwnPromise.race('').then(() => unexpectedResolve(done), () => unexpectedReject(done))
        .then(done)
        .catch(done);
      done();
    });

    it('not empty string in method should return a fulfilled promise with result of first character', function(done) {
      OwnPromise.race('string').then(res => {
        expect(res).to.equal('s');
      }, () => unexpectedReject(done))
        .then(done)
        .catch(done);
    });
  });
  describe('method should work with undefined or null', function(done) {
    it('result of returned fulfillded promise should be undefined', function(done) {
      OwnPromise.race([undefined]).then(function(resolved) {
        expect(resolved).to.be.undefined;
      }, () => unexpectedReject(done))
        .then(done)
        .catch(done);
    });

    it('result of returned fulfillded promise should be null', function(done) {
      OwnPromise.race([undefined]).then(function(resolved) {
        expect(resolved).to.be.undefined;
      }, () => unexpectedReject(done))
        .then(done)
        .catch(done);
    });
  });
});