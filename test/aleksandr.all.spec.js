const { assert, expect, done } = require('chai');
const OwnPromise = require('../src/promise');

function unexpectedReject(done) {
  return function() {
    done('Error: promise was expected to not reject, but did.');
  };
}

function unexpectedResolve(done) {
  return function() {
    done('Error: promise was expected to not resolve, but did.');
  };
}

describe('method all', function(done) {
  let myRes1 = null;
  const p = new OwnPromise(res => {
    myRes1 = res;
  });
  let myRes2 = null;
  const p2 = new OwnPromise(res => {
    myRes2 = res;
  });
  const ourIterable = [p, p2];
  ourIterable.length = 10;

  it('returned promise should be resolved', function(done) {
    OwnPromise.all(ourIterable).then(function(resolved) {
      assert.equal(1, 1);
    }, () => unexpectedReject(done))
      .then(done)
      .catch(done);
    myRes1(1);
    myRes2(2);
  });

  it('result of returned promise should be an array', function(done) {
    OwnPromise.all(ourIterable).then(res => {
      expect(res).to.be.an('array');
    })
      .catch(done);
    myRes1(1);
    myRes2(2);
    done();
  });

  it('the length of array of result should be 10', function(done) {
    OwnPromise.all(ourIterable).then(function(resolved) {
      expect(resolved).to.has.length(10);
    }, () => unexpectedReject(done))
      .then(done)
      .catch(done);
    myRes1(1);
    myRes2(2);
  });

  it('first and second items of array of results should be results of resolved promise', function(done) {
    OwnPromise.all(ourIterable).then(function(resolved) {
      expect(resolved[0]).to.equal(1);
      expect(resolved[1]).to.equal(2);
    }, () => unexpectedReject(done))
      .then(done)
      .catch(done);
    myRes1(1);
    myRes2(2);
  });

  // eslint-disable-next-line max-len
  it('empty items of the array that is passed to the method will be passed to the result array as undefined', function(done) {
    OwnPromise.all(ourIterable).then(function(resolved) {
      expect(resolved[5]).to.be.undefined;
      expect(resolved[8]).to.be.undefined;
    }, () => unexpectedReject(done))
      .then(done)
      .catch(done);
    myRes1(1);
    myRes2(2);
  });
  describe('method should work with strings', function(done) {
    it('empty string', function(done) {
      OwnPromise.all('').then(function(resolved) {
        expect(resolved).to.exist;
      }, () => unexpectedReject(done))
        .then(done)
        .catch(done);
    });

    it('not empty string', function(done) {
      OwnPromise.all('some string').then(function(resolved) {
        expect(resolved).to.exist;
      }, () => unexpectedReject(done))
        .then(done)
        .catch(done);
    });

    it('array of results should contain characters of passed string', function(done) {
      OwnPromise.all('string').then(function(resolved) {
        expect(resolved[0]).to.equal('s');
        expect(resolved[2]).to.equal('r');
        expect(resolved[4]).to.equal('n');
      }, () => unexpectedReject(done))
        .then(done)
        .catch(done);
    });
  });

  it('should throw Error', function(done) {
    const notRealIterable = { length: 0 };
    OwnPromise.all(notRealIterable)
      .then(unexpectedReject(done))
      .catch(function(err) {
        assert.ok(err instanceof TypeError);
      })
      .then(done)
      .catch(done);
  });
});