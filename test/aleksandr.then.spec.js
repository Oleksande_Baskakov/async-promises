const { assert, expect, done } = require('chai');
const OwnPromise = require('../src/promise');

describe('25.4.5.3 Promise.prototype.then', function() {
  it('is a function', function() {
    assert.isFunction(OwnPromise.prototype.then);
  });
  it('expects "this" to be a Promise', function() {
    let that = null;
    const promise = new OwnPromise(() => {
      that = this;
    });
    assert.ok(that.constructor, OwnPromise);
  });
  it('catch take one argument', function() {
    assert.equal(OwnPromise.prototype.catch.length, 1);
  });
});